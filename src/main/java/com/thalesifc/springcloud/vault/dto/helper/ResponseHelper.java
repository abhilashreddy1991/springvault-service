package com.thalesifc.springcloud.vault.dto.helper;

import org.springframework.http.HttpStatus;

import com.thalesifc.springcloud.vault.dto.ResponseDto;
import com.thalesifc.springcloud.vault.dto.StatusDto;

public class ResponseHelper {
	
	public static ResponseDto getSuccessResponse() {
		StatusDto statusDto = new StatusDto();
		statusDto.setCode(HttpStatus.OK.value());
		ResponseDto responseDto = new ResponseDto();
		responseDto.setStatus(statusDto);
		return responseDto;
	}

	public static ResponseDto getErrorResponse(int statusCode) {
		StatusDto statusDto = new StatusDto();
		statusDto.setCode(statusCode);
		ResponseDto responseDto = new ResponseDto();
		responseDto.setStatus(statusDto);
		return responseDto;
	}

}
