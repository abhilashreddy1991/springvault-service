package com.thalesifc.springcloud.vault.dto;


public class ResponseDto {
	private StatusDto status;	
	private Object data;
	public StatusDto getStatus() {
		return status;
	}
	public void setStatus(StatusDto status) {
		this.status = status;
	}
	public Object getData() {
		return data;
	}
	public void setData(Object data) {
		this.data = data;
	}
	
	

}
