package com.thalesifc.springcloud.vault.dto;

import java.util.List;


public class StatusDto {
	
	private int code; //http status code
	private List<ErrorinfoDto> errors; //error info
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}

	public List<ErrorinfoDto> getErrors() {
		return errors;
	}
	public void setErrors(List<ErrorinfoDto> errors) {
		this.errors = errors;
	}
	
	

}
