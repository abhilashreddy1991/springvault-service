package com.thalesifc.springcloud.vault.service;


import org.springframework.vault.support.VaultResponse;



public interface SpringVaultService {
	/**
	 * 
	 * @param path
	 * @return
	 */
	public VaultResponse getAllSecrets(String path);
	
	/**
	 * 
	 * @param path
	 */
	public void deleteSecret(String path);	
	/**
	 * 
	 * @param file
	 * @param path
	 * @return
	 */
	public VaultResponse postSecret(Object secretsJson, String path);

	/**
	 * 
	 * @param file
	 * @param path
	 * @return
	 */
	public VaultResponse updateSecret(Object secretJson, String path);

}
