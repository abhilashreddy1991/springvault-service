package com.thalesifc.springcloud.vault.service;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.vault.core.VaultTemplate;
import org.springframework.vault.support.VaultResponse;

import com.thalesifc.springcloud.vault.configuration.VaultConfig;

@Service
public class SpringVaultServiceImpl implements SpringVaultService{
	
	@Autowired
	VaultConfig config;

	@Override
	public VaultResponse getAllSecrets(String path) {
		VaultTemplate vaultTemplate = new VaultTemplate(config.vaultEndpoint(), config.clientAuthentication());
		VaultResponse response = vaultTemplate.read(path);
		return response;
	}

	@Override
	public void deleteSecret(String path) {
		VaultTemplate vaultTemplate = new VaultTemplate(config.vaultEndpoint(), config.clientAuthentication());
		vaultTemplate.delete(path);
	}

	@Override
	public VaultResponse postSecret(Object secretsJson, String path) {
		VaultTemplate vaultTemplate = new VaultTemplate(config.vaultEndpoint(), config.clientAuthentication());
		vaultTemplate.write(path, secretsJson);
		VaultResponse response = vaultTemplate.read(path);
		return response;
	}

	@Override
	public VaultResponse updateSecret(Object secretJson, String path) {
		VaultTemplate vaultTemplate = new VaultTemplate(config.vaultEndpoint(), config.clientAuthentication());
		vaultTemplate.write(path, secretJson);
		VaultResponse response = vaultTemplate.read(path);
		return response;
	}

}
