package com.thalesifc.springcloud.vault;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@EnableDiscoveryClient
@SpringBootApplication
@ComponentScan(basePackages = { "com.thalesifc.springcloud" })
public class SpringVaultApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringVaultApplication.class, args);
    }
	
}
