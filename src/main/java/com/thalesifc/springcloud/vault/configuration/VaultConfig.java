package com.thalesifc.springcloud.vault.configuration;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.env.Environment;
import org.springframework.vault.authentication.ClientAuthentication;
import org.springframework.vault.authentication.TokenAuthentication;
import org.springframework.vault.client.VaultEndpoint;
import org.springframework.vault.config.AbstractVaultConfiguration;

@Configuration
//@PropertySource(value = { "file:src/main/resources/bootstrap.properties" })
public class VaultConfig extends AbstractVaultConfiguration {
	
	@Autowired
	Environment env;

	@Value("${spring.cloud.vault.host}")
	private String localhost;
	@Value("${spring.cloud.vault.port}")
	private int port;
	@Value("${spring.cloud.vault.scheme}")
	private String scheme;
	@Value("${spring.cloud.vault.token}")
	private String token;
	
	@Override
	public VaultEndpoint vaultEndpoint() {
		VaultEndpoint endpoint = new VaultEndpoint();
		endpoint.setHost(localhost);
		endpoint.setPort(port);
		endpoint.setScheme(scheme);
		
		return endpoint;
	}

	@Override
	public ClientAuthentication clientAuthentication() {
		
		return new TokenAuthentication(token);
	}
	@Bean
	public static PropertySourcesPlaceholderConfigurer propertyconfig(){
		return new PropertySourcesPlaceholderConfigurer();
		
	}

}
