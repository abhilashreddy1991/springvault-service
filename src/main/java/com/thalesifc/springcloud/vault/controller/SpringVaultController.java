package com.thalesifc.springcloud.vault.controller;




import javax.servlet.http.HttpServletRequest;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.vault.support.VaultResponse;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.thalesifc.springcloud.vault.dto.ResponseDto;
import com.thalesifc.springcloud.vault.dto.helper.ResponseHelper;
import com.thalesifc.springcloud.vault.service.SpringVaultService;

@RestController
@RequestMapping(value ="/vault/tapi/v1/secret/")
public class SpringVaultController {

	@Autowired
	SpringVaultService vaultService;

	@RequestMapping(value = "/**", method = RequestMethod.GET, 
			produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<ResponseDto> getSecret(HttpServletRequest request) {

		try {
		
			String query =request.getQueryString();
			String requestURI = request.getRequestURI();
			String prefix = "/vault/tapi/v1";
			String path = requestURI.substring(requestURI.indexOf(prefix) + prefix.length());
//			String path1 = path + list;

			VaultResponse response = vaultService.getAllSecrets(path + "?" + query);
			ResponseDto responseDto = ResponseHelper.getSuccessResponse();
			responseDto.setData(response);
			return new ResponseEntity<ResponseDto>(responseDto, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<ResponseDto>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/**", method = RequestMethod.DELETE)
	public ResponseEntity<String> deleteSecret(HttpServletRequest request) {

		try {
			String requestURI = request.getRequestURI();
			String prefix = "/vault/tapi/v1";
			String path = requestURI.substring(requestURI.indexOf(prefix) + prefix.length());
			vaultService.deleteSecret(path);
			return ResponseEntity.status(HttpStatus.OK).body("Secret deleted if it existed");
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Error processing request");
		}

	}

	@RequestMapping(value = "/**", method = RequestMethod.POST, consumes = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<ResponseDto> writeSecret( HttpServletRequest request, 
			@RequestBody Object secretJson) {
		try {
			String requestURI = request.getRequestURI();
			String prefix = "/vault/tapi/v1";
			String path = requestURI.substring(requestURI.indexOf(prefix) + prefix.length());
			

			VaultResponse postResponse = vaultService.postSecret(secretJson, path);
			ResponseDto responseDto = ResponseHelper.getSuccessResponse();
			responseDto.setData(postResponse);

			return new ResponseEntity<ResponseDto>(responseDto, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<ResponseDto>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@RequestMapping(value = "/**", method = RequestMethod.PUT, consumes = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<ResponseDto> updateSecret( HttpServletRequest request, 
			@RequestBody Object secretJson) {
		try {
			String requestURI = request.getRequestURI();
			String prefix = "/vault/tapi/v1";
			String path = requestURI.substring(requestURI.indexOf(prefix) + prefix.length());

			VaultResponse postResponse = vaultService.updateSecret(secretJson, path);
			ResponseDto responseDto = ResponseHelper.getSuccessResponse();
			responseDto.setData(postResponse);

			return new ResponseEntity<ResponseDto>(responseDto, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<ResponseDto>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}