package com.thalesifc.springcloud.vault.service;

import static org.mockito.Matchers.*;
import static org.mockito.BDDMockito.*;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.vault.core.VaultTemplate;
import org.springframework.vault.support.VaultResponse;

import com.thalesifc.springcloud.vault.SpringVaultApplication;
import com.thalesifc.springcloud.vault.configuration.VaultConfig;

//@RunWith(SpringRunner.class)
//@SpringBootTest(classes = SpringVaultApplication.class)
@WebAppConfiguration
@TestPropertySource({ "file:src/test/resources/vault-service-test.properties" })
public class SpringValutServiceTest {

	private SpringVaultService springVaultService;
	
	@Mock
	private VaultTemplate vaultTemplate;
	
	@Mock
	//@InjectMocks
	private VaultConfig vaultConfig;
	
	@Before
	public void setup() {
		springVaultService = new SpringVaultServiceImpl();
		ReflectionTestUtils.setField(springVaultService, "vaultTemplate", vaultTemplate);
		ReflectionTestUtils.setField(springVaultService, "vaultConfig", vaultConfig);
	}
	
//	@Test
//	public void testGetAllSecrets() {
//		given(vaultTemplate.read(anyString())).willReturn(new VaultResponse());//data setup
//		VaultResponse response = springVaultService.getAllSecrets("/abcd"); // call
//	
//		Assert.assertEquals(response.getData(), "");
//	}
}
